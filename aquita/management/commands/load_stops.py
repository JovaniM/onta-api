from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
import argparse
import json

from aquita.models import BusStop


class Command(BaseCommand):
    help = "Imports stops from a single feature collection, obtained from OSM \
            via overpass api"""

    def add_arguments(self, parser):
        parser.add_argument('filename', type=argparse.FileType('r'))

    def handle(self, *args, **options):
        data = json.loads(options['filename'].read())

        ncreated = 0
        nupdated = 0

        for stop in data['elements']:
            _, created = BusStop.objects.update_or_create(
                osm_node_id=stop['id'], defaults={
                    "point": Point(x=stop['lon'], y=stop['lat']),
                    "name": stop['tags'].get('name'),
                    "source": 'osm',
                },
            )

            if created:
                ncreated += 1
            else:
                nupdated += 1

            print('.', end='', flush=True)
        print()

        print('{} created, {} updated'.format(ncreated, nupdated))
