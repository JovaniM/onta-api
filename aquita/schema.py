from graphql_geojson import GeoJSONType
from .models import BusStop, BusRoute, BusTag
from graphene import Node
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django import DjangoObjectType
from .filters import BusRouteFilter


class BusTagNode(DjangoObjectType):

    class Meta:
        model = BusTag
        interfaces = (Node,)
        filter_fields = {
            'name': ('exact', 'icontains'),
        }


class BusRouteNode(GeoJSONType):

    class Meta:
        model = BusRoute
        interfaces = (Node,)
        geojson_field = 'route'


class BusStopNode(GeoJSONType):

    class Meta:
        model = BusStop
        interfaces = (Node,)
        geojson_field = 'point'
        filter_fields = ('name',)


class Query(object):
    bus_tag = Node.Field(BusTagNode)
    all_bus_tags = DjangoFilterConnectionField(BusTagNode)

    bus_route = Node.Field(BusRouteNode)
    all_bus_routes = DjangoFilterConnectionField(
        BusRouteNode, filterset_class=BusRouteFilter,
    )

    bus_stop = Node.Field(BusStopNode)
    all_bus_stops = DjangoFilterConnectionField(BusStopNode)
