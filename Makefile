.PHONY: test pytest lint

test: pytest lint

pytest:
	pytest

lint:
	flake8 --exclude=.env,.tox,dist,docs,build,migrations,*.egg .
