# Generated by Django 2.1.2 on 2019-05-24 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aquita', '0010_busstop_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='busstop',
            name='mapaton_id',
            field=models.BigIntegerField(blank=True, default=None, help_text='Id de parada según como fue extraída de los archivos del mapatón', null=True, unique=True),
        ),
    ]
